package graph

import "errors"

// Нода ориентированного графа
type Node[V any] struct {

	// Значение ассоциированное с нодой
	Value V

	// Соседние ноды
	siblings []*Node[V]
}

// Добавить связь данной ноды с другой
func (n *Node[V]) ConnectTo(node *Node[V]) {
	if n.siblings == nil {
		n.siblings = make([]*Node[V], 0, 1)
	}

	n.siblings = append(n.siblings, node)
}

// Добавляет новую дочернюю ноду и возвращает ссылку на нее
func (n *Node[V]) AddSibling() *Node[V] {
	node := &Node[V]{}

	n.ConnectTo(node)

	return node
}

// Обходит последовательно дочерние ноды, вызывая для каждой функцию
func (n *Node[V]) MapSiblings(handler func(node *Node[V]) bool) {
	for nodeIndex := range n.siblings {
		if handler(n.siblings[nodeIndex]) {
			return
		}
	}
}

func (n *Node[V]) bfs_internal(handler func(node *Node[V]) bool, excludeSelf bool) bool {
	if !excludeSelf && handler(n) {
		return true
	}
	for nodeIndex := range n.siblings {
		if handler(n.siblings[nodeIndex]) {
			return true
		}
	}

	for nodeIndex := range n.siblings {
		if n.siblings[nodeIndex].bfs_internal(handler, true) {
			return true
		}
	}

	return false
}

// Обходит ориентированный граф поиском в ширину (сначала все непосредственные связи, потом в глубину и т.п.)
// Если handler возвращает true то это означает что поиск окончен и дальнейший обход нод прекращается
func (n *Node[V]) Bfs(handler func(node *Node[V]) bool) bool {
	return n.bfs_internal(handler, false)
}

// Возвращает соседнюю ноду только в случае если она является единственной или nil если соседних нод нет.
// В противном случае возвращает ошибку
func (n *Node[V]) OnlySibling() (*Node[V], error) {
	if len(n.siblings) == 1 {
		return n.siblings[0], nil
	}

	if n.siblings == nil || len(n.siblings) == 0 {
		return nil, nil
	}

	return nil, errors.New("у ноды должен быть единственный сосед или не должно их быть вообще")
}
