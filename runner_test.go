package saga

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestRunner(t *testing.T) {
	t.Log("Тест работы с переменными в саге")

	r := &Runner{}

	r.SetVar("a", "some")

	require.Equal(t, "some", r.GetVar("a"))
	require.Nil(t, r.GetVar("non-exist"))
}

func TestSagaWithVars(t *testing.T) {
	t.Log("Тесты Runner в режиме локальных переменных")

	saga := New()
	saga.UseVars()

	saga.RootBranchP("step1", func(a any) (*ProcessResult, error) {
		require.IsType(t, (*VarsModeParams)(nil), a)
		a.(*VarsModeParams).Vars["p"] = 8
		return nil, nil
	}, nil).P("last", func(a any) (*ProcessResult, error) {
		require.Equal(t, 8, a.(*VarsModeParams).Vars["p"])
		require.Equal(t, nil, a.(*VarsModeParams).TraversalParams)
		return nil, nil
	}, nil)
	saga.Seal()

	runner := saga.NewRunner()
	runner.Run(nil)
}
