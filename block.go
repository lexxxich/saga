package saga

// Различные блоки диаграммы потока, из которых состоит Fsm

const (
	// Блок этого считается ошибкой
	BlockTypeInvalid int = iota

	//Блок этого типа выполняет определенную задачу, указанную в Executor
	BlockTypeProcess

	// Условный блок
	BlockTypeIf
)

type Block struct {
	Id          string // Уникальный идентификатор ноды в Fsm. Нужен для идентификации условных переходов
	Type        int
	Executor    func(any) (*ProcessResult, error)
	Compensator func(any) error

	LinkTo []string // Дополнительные связи, которые нужно организовать при компоновке графа
}
