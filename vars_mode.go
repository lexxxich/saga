package saga

type VarsModeParams struct {

	// Глобальные переменные Runner-а
	Vars map[string]any

	// Параметры, переданные из предыдущего шага саги или изначальные входные параметры саги, переданные клиентом
	TraversalParams any
}

// Извлекает переменную из хранилища переменных Runner-а.
//
// Сага должна быть в режиме глобальных переменных
func GetVar[T any](store any, name string) (T, error) {
	vars, ok := store.(*VarsModeParams)
	if !ok {
		var errResult T
		return errResult, ErrSagaNotInVarsMode
	}

	v, ok := vars.Vars[name].(T)
	if !ok {
		var errResult T
		return errResult, ErrNoSuchVariable

	}

	return v, nil
}

// Задает или заменяет значение переменной саги
//
// Сага должна быть в режиме переменных
func SetVar[T any](store any, name string, value T) error {
	vars, ok := store.(*VarsModeParams)
	if !ok {
		return ErrSagaNotInVarsMode
	}

	vars.Vars[name] = value

	return nil
}
