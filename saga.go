package saga

import (
	"errors"
	"fmt"

	"gitlab.com/lexxxich/saga/graph"
)

var (
	ErrInvalidBlockType             = errors.New("invalid block type")
	ErrInvalidRoot                  = errors.New("root node wasnt initialized")
	ErrCondExecutorMustReturnString = errors.New("executor условной ноды должен вернуть ид ноды, на которую нужно перейти")
	ErrNoTransitionNode             = errors.New("условный блок не вернул ноду, на которую следует перейти")
	ErrNothingToRun                 = errors.New("отсутствуют шаги для выполнения машины")
	ErrSagaNotInVarsMode            = errors.New("метод-исполнитель использует режим глобальных переменных саги, но данный режим сагой	 не предусмотрен")
	ErrNoSuchVariable               = errors.New("запрошенная глобальная переменная не существует или некорректного типа")
)

// Template - конечный автомат с поддержкой ветвлений, реализующий паттерн Саги на базе оркестрации.
type Template struct {
	rootNode *graph.Node[Block]

	islands []*FsmBranch // Ссылки на изолированные ветки

	// Использовать или нет локальные переменные Runner при работе саги.
	// Если FALSE то все executor саги должны возвращать результат своей работы, который по цепочке передается
	// следующим шагам
	useVars bool
}

func New() *Template {
	return &Template{
		useVars: false,
	}
}

// Использовать или нет локальные переменные Runner при работе саги.
// Если FALSE то все executor саги должны возвращать результат своей работы, который по цепочке передается
// следующим шагам
func (f *Template) UseVars() {
	f.useVars = true
}

// NewRunner создает объект-исполнитель процессов, описанных в этом Fsm (конечном автомате)
// Используя Runner можно запустить выполнение цепочки действий, а также, в случае ошибок
// при ее выполнении - запустить цепочку компенсаторных действий.
func (f *Template) NewRunner() *Runner {
	return &Runner{
		template:            f,
		cursor:              f.rootNode,
		compensateStepIndex: 0,
		vars:                make(map[string]any),
	}
}

// ВОзвращает стартовый блок машины
func (f *Template) createRootNodeProcess() *graph.Node[Block] {
	if f.rootNode == nil {
		f.rootNode = &graph.Node[Block]{}
		f.rootNode.Value.Type = BlockTypeProcess
		f.rootNode.Value.Executor = nil
	}

	return f.rootNode
}

// Создает изолированную (не связанную с машиной Fsm) ветку процессов, начинающуюся с процессной ноды
// Завершать ветку допускается процессной нодой, или условным переходом.
//
// В этом случае ветки, на которые возможен переход из условной ноды необходимо будет связать между собой методом Link.
//
// # Параметры
//
// - id: Уникальный идентификатор (название) процесса
//
// - executor - функция-исполнитель процесса. В ней находится вся логика обработки входных параметров, переданных предыдущим процессом
// формирование пакета параметров для осуществления компенсаторного действия и параметров, передаваемых следующей
// процессной или условной ноде. функции-исполнителю передается объект с входными параметрами, сформированными либо предыдущей нодой,
// либо полученными извне и переданными методу Fsm.Run(input).
//
// - compensator - функция-исполнитель компенсаторного действия для данного процесса. Получает на вход параметры компенсации,
// сгенерированные функцией-исполнителем.
func (f *Template) IsolatedBranchP(id string, executor func(any) (*ProcessResult, error), compensator func(any) error) *FsmBranch {
	node := &graph.Node[Block]{}
	node.Value.Type = BlockTypeProcess
	node.Value.Id = id
	node.Value.Executor = executor
	node.Value.Compensator = compensator

	rv := &FsmBranch{
		fsm:      f,
		lastNode: node,
		root:     node,
	}

	if f.islands == nil {
		f.islands = make([]*FsmBranch, 0)
	}

	f.islands = append(f.islands, rv)

	return rv
}

// Создает изолированную (не связанную с машиной Fsm) ветку, начинающуюся с условной ноды
//
// # Параметры
//
// - id: Уникальный идентификатор (название) процесса или условной ноды
//
// - id: Уникальный идентификатор (название) условной ноды
//
// - executor - функция-исполнитель условия. В ней находится вся логика обработки входных параметров, переданных предыдущим процессом
//
// - массив идентификаторов нод, к которым потенциально может осуществиться условный переход
//
// Эта функция должна вернуть уникальный идентификатор ноды, на которую необходимо осуществить переход.
func (f *Template) IsolatedBranchC(id string, executor func(any) (*ProcessResult, error), connections []string) *FsmBranch {
	node := &graph.Node[Block]{}
	node.Value.Type = BlockTypeProcess
	node.Value.Id = id
	node.Value.Executor = executor
	node.Value.Compensator = nil
	node.Value.LinkTo = connections

	rv := &FsmBranch{
		fsm:      f,
		lastNode: node,
		root:     node,
	}

	if f.islands == nil {
		f.islands = make([]*FsmBranch, 0)
	}

	f.islands = append(f.islands, rv)

	return rv
}

// Создает корневую (главную) ветку, привязанную к машине состояний, начинающуюся с процессной ноды
// Завершать ветку допускается процессной нодой, или условным переходом.
//
// # Параметры
//
// - id: Уникальный идентификатор (название) процесса или условной ноды
//
// - executor - функция-исполнитель процесса. В ней находится вся логика обработки входных параметров, переданных предыдущим процессом
// формирование пакета параметров для осуществления компенсаторного действия и параметров, передаваемых следующей
// процессной или условной ноде. функции-исполнителю передается объект с входными параметрами, сформированными либо предыдущей нодой,
// либо полученными извне и переданными методу Fsm.Run(input).
//
// Если сага объявлена с использованием режима переменных (UseVars()), то в качестве параметров в функцию-исполнитель передается
// Ссылка на структуру VarsModeParams, содержащую как ссылку на внутренние переменные Runner а саги, так и объект, полученный
// на ее предыдущем шаге.
//
// - compensator - функция-исполнитель компенсаторного действия для данного процесса. Получает на вход параметры компенсации,
// сгенерированные функцией-исполнителем.
func (f *Template) RootBranchP(id string, executor func(any) (*ProcessResult, error), compensator func(any) error) *FsmBranch {
	node := f.createRootNodeProcess()
	node.Value.Type = BlockTypeProcess
	node.Value.Id = id
	node.Value.Executor = executor
	node.Value.Compensator = compensator

	return &FsmBranch{
		fsm:      f,
		lastNode: node,
		root:     node,
	}
}

// Создает корневую (главную) ветку, привязанную к машине состояний, начинающуюся с условной ноды
//
// # Параметры
//
// - id: Уникальный идентификатор (название) условной ноды
//
// - executor - функция-исполнитель условия. В ней находится вся логика обработки входных параметров, переданных предыдущим процессом
//
// - массив идентификаторов нод, к которым потенциально может осуществиться условный переход
//
// Эта функция должна вернуть уникальный идентификатор ноды, на которую необходимо осуществить переход.
func (f *Template) RootBranchC(id string, executor func(any) (*ProcessResult, error), connections []string) *FsmBranch {
	node := f.createRootNodeProcess()
	node.Value.Type = BlockTypeIf
	node.Value.Id = id
	node.Value.Executor = executor
	node.Value.Compensator = nil
	node.Value.LinkTo = connections

	return &FsmBranch{
		fsm:      f,
		lastNode: node,
		root:     node,
	}
}

// Завершает дизайн саги
func (f *Template) Seal() {

	if f.islands == nil || len(f.islands) == 0 {
		return
	}

	roots := make([]*graph.Node[Block], len(f.islands)+1)
	roots[0] = f.rootNode
	for islandIndex := range f.islands {
		roots[islandIndex+1] = f.islands[islandIndex].root
	}

	for rootIndex := range roots {

		roots[rootIndex].Bfs(func(node *graph.Node[Block]) bool {
			if node.Value.LinkTo == nil || len(node.Value.LinkTo) == 0 {
				return false
			}

			for toNodeIndex := range node.Value.LinkTo {
				toNodeId := node.Value.LinkTo[toNodeIndex]
				toNodeRef := f.getNodeGlobal(toNodeId, roots)

				if toNodeRef == nil {
					panic(fmt.Sprintf("Компиляция саги не возможна. Отсутствует нода [%s] для связи с [%s]", toNodeId, node.Value.Id))
				}

				node.ConnectTo(toNodeRef)
			}

			node.Value.LinkTo = nil // Чистим память

			return false
		})
	}

}

func (f *Template) getNodeGlobal(nodeName string, roots []*graph.Node[Block]) *graph.Node[Block] {

	var rvNode *graph.Node[Block]

	for rootIndex := range roots {
		roots[rootIndex].Bfs(func(node *graph.Node[Block]) bool {
			if node.Value.Id == nodeName {
				rvNode = node
				return true
			}
			return false
		})

	}

	return rvNode

}

// Выполняет шаг машины состояний
// Если машина завершила работу то возвращается true. Если есть еще шиги - false
func (f *Template) step(params any, cur *graph.Node[Block], vars map[string]any) (*graph.Node[Block], *Compensate, any, error) {

	var err error
	// var nodeReturn any
	// var compensatePrms any
	var processResult *ProcessResult
	var nextNode *graph.Node[Block]

	var cursor *graph.Node[Block]

	// Если процесс выполнения саги еще не начался, то устанавливаем курсор на корневую ноду
	if cur == nil {
		if f.rootNode == nil {
			return nil, nil, nil, ErrInvalidRoot
		}
		cursor = f.rootNode
	} else {
		cursor = cur
	}

	if cursor.Value.Type == BlockTypeInvalid {
		return nil, nil, nil, ErrInvalidBlockType
	}

	// Обрабатываем процессную ноду
	if cursor.Value.Type == BlockTypeProcess {
		var stepParams = params
		if f.useVars {
			stepParams = &VarsModeParams{
				Vars:            vars,
				TraversalParams: params,
			}
		}
		processResult, err = cursor.Value.Executor(stepParams)
		if err != nil {
			return nil, nil, nil, errors.Join(err, fmt.Errorf("Шаг саги: %s", cursor.Value.Id))
		}

		// Если исполнитель ничего не вернул, эмулируем возврат пустого результата
		if processResult == nil {
			processResult = &ProcessResult{}
		}

		if processResult.MustTerminate {
			nextNode = nil
		} else {
			nextNode, err = cursor.OnlySibling()
		}

		if err != nil {
			return nil, nil, nil, errors.Join(err, fmt.Errorf("Шаг саги: %s", cursor.Value.Id))
		}

		compensateFn := cursor.Value.Compensator
		if compensateFn == nil {
			return nextNode, nil, processResult.Value, nil
		}

		compensator := &Compensate{
			Params:   processResult.CompensateParams,
			Executor: compensateFn,
		}

		return nextNode, compensator, processResult.Value, nil
	}

	// Условный переход на следующую ноду
	if cursor.Value.Type == BlockTypeIf {
		if cursor.Value.Executor == nil {
			return nil, nil, nil, ErrCondExecutorMustReturnString
		}
		var stepParams = params
		if f.useVars {
			stepParams = &VarsModeParams{
				Vars:            vars,
				TraversalParams: params,
			}
		}
		processResult, err = cursor.Value.Executor(stepParams)
		// nodeReturn, _, err = cursor.Value.Executor(params)

		if err != nil {
			return nil, nil, nil, errors.Join(err, fmt.Errorf("Шаг саги: %s", cursor.Value.Id))
		}

		next, ok := processResult.Value.(string)
		if !ok {
			return nil, nil, nil, ErrCondExecutorMustReturnString
		}

		// Если исполнитель ничего не вернул, эмулируем возврат пустого результата
		if processResult == nil {
			processResult = &ProcessResult{}
		}

		if processResult.MustTerminate {
			nextNode = nil
		} else {
			cursor.MapSiblings(func(node *graph.Node[Block]) bool {
				if node.Value.Id == next {
					nextNode = node
					return true
				}
				return false
			})
		}

		if nextNode == nil {
			return nil, nil, nil, ErrNoTransitionNode
		}

		return nextNode, nil, params, nil

	}

	return nil, nil, nil, ErrInvalidBlockType

}

// Соединяет корневую ветку машины состояний (Fsm) и ветку, указанную в dstBranch между собой.
//
// fromNode - содержит уникальный идентификатор ноды, находящийся в исходной ветви
//
// toNodes - содержит список уникальных идентификатров ветвей, с которыми соединяется исходная.
// Если исходная ветвь соединяется с несколькими исходными, то исходная нода должна быть условного типа (не процессная).
// В противном случае результат перехода от исходной до конечной ветвей непредсказуем.
// func (f *Template) MergeNodeWithBranch(nodeName string, toBranch *FsmBranch) {
// 	var srcNode *graph.Node[Block] = f.getNode(nodeName)

// 	if srcNode == nil {
// 		panic(fmt.Sprintf("Нода [%s] для связи не найдена", nodeName))
// 	}

// 	dstNode := toBranch.root
// 	srcNode.ConnectTo(dstNode)
// }

// func (f *Template) ConnectNodes(fromNode string, toNode string) {
// 	from := f.getNode(fromNode)
// 	if from == nil {
// 		panic(fmt.Sprintf("Нода [%s] для связи не найдена", fromNode))
// 	}

// 	to := f.getNode(toNode)
// 	if to == nil {
// 		panic(fmt.Sprintf("Нода [%s] для связи не найдена", toNode))
// 	}

// 	from.ConnectTo(to)
// }

func (f *Template) getNode(id string) *graph.Node[Block] {
	var foundNode *graph.Node[Block] = nil

	f.rootNode.Bfs(func(node *graph.Node[Block]) bool {
		if node.Value.Id == id {
			foundNode = node
			return true
		}
		return false
	})

	return foundNode
}

type FsmBranch struct {
	fsm      *Template
	root     *graph.Node[Block]
	lastNode *graph.Node[Block]
}

// Добавляет к ветке новую процессную ноду (в конец ветки).
//
// # Параметры
//
// - id: Уникальный идентификатор (название) процесса
//
// - executor - функция-исполнитель процесса. В ней находится вся логика обработки входных параметров, переданных предыдущим процессом
// формирование пакета параметров для осуществления компенсаторного действия и параметров, передаваемых следующей
// процессной или условной ноде. функции-исполнителю передается объект с входными параметрами, сформированными либо предыдущей нодой,
// либо полученными извне и переданными методу Fsm.Run(input).
//
// - compensator - функция-исполнитель компенсаторного действия для данного процесса. Получает на вход параметры компенсации,
// сгенерированные функцией-исполнителем.
func (b *FsmBranch) P(id string, executor func(any) (*ProcessResult, error), compensator func(any) error) *FsmBranch {

	nextProcess := b.lastNode.AddSibling()
	nextProcess.Value.Type = BlockTypeProcess
	nextProcess.Value.Id = id
	nextProcess.Value.Executor = executor
	nextProcess.Value.Compensator = compensator

	b.lastNode = nextProcess

	return b
}

// Добавляет к ветке новую условную ноду (в конец ветки).
//
// # Параметры
//
// - id: Уникальный идентификатор (название) условной ноды
//
// - executor - функция-исполнитель условия. В ней находится вся логика обработки входных параметров, переданных предыдущим процессом
//
// - массив идентификаторов нод, к которым потенциально может осуществиться условный переход
//
// Эта функция должна венуть уникальный идентификатор ноды, на которую необходимо осуществить переход.
func (b *FsmBranch) C(id string, executor func(any) (*ProcessResult, error), connections []string) *FsmBranch {
	cond := b.lastNode.AddSibling()
	cond.Value.Type = BlockTypeIf
	cond.Value.Id = id
	cond.Value.Executor = executor
	cond.Value.Compensator = nil
	cond.Value.LinkTo = connections

	b.lastNode = cond

	return b
}

// Присоединяет данную ветку к указанной ноде корневой ветки
// Для присоединения используется последняя нода в данной ветке в качестве источника и указанная нода в главной ветке в качестве пункта назначения
//
// Схематично: [Ветка].[Последняя нода] --> [Главная ветка].[Указанная нода (rootNodeName)]
func (b *FsmBranch) MergeWithRoot(rootNodeName string) {

	rootNodeRef := b.fsm.getNode(rootNodeName)
	if rootNodeRef == nil {
		panic(fmt.Sprintf("Нода [%s] не найдена в главной ветке", rootNodeName))
	}

	b.lastNode.ConnectTo(rootNodeRef)

}
