package saga

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestStep(t *testing.T) {
	f := &Template{}
	runner := f.NewRunner()

	_, _, err := runner.Step(nil)
	require.ErrorIs(t, err, ErrInvalidRoot)

	f.RootBranchP("node", func(a any) (*ProcessResult, error) { return nil, nil }, nil)

	runner = f.NewRunner()
	_, _, err = runner.Step(nil)
	t.Log("Должен вызываться Executor ноды!!!")
	require.NoError(t, err)

	_, err = runner.Run(nil)
	require.Error(t, err)

}

func TestExample(t *testing.T) {
	t.Log("Пример организаии кода по управлению машиной")
	t.Log("В этом примере мы будем сохранть задачу-тодошку в базу данных, содержащую вложенные файлы, с периодическим напоминанием.")
	t.Log("Предполагается что на вход поступает структура с данными о сохраняемой тодошке")

	input := &InputTodoData{
		Title:          "Покормить кота",
		TmpFiles:       []string{"/tmp/cat_photo.jpg", "/tmp/food_photo.png"},
		Invoker:        "Сын",
		RepeatTypeName: "ежедневно",
	}

	template := &Template{}

	// Главный процесс. Заканчиваем его ветвлением, чтобы понимать куда двигаться дальше
	template.RootBranchP("validateInput", validateInputProcessExecutor, nil).
		C("saveOrProceed", condSaveFilesOrSkipExecutor,[]string{"saveFile","saveDeferredAction"})

	template.
		IsolatedBranchP("saveFile", saveFilesPermanentlyExecutor, saveFilePermanentlyCompensator).
		P("saveDeferredAction", saveDeferredActionExecutor, nil).
		P("saveTodo", saveTodoExecutor, nil).
		P("notifyInvoker", notifyInvokerExecutor, nil)


	template.Seal()

	_, err := template.NewRunner().Run(input)

	require.NoError(t, err)

}

func TestFailFsm(t *testing.T) {
	t.Log("В этом примере машина завершает работу с ошибкой")
	t.Log("В результате возврата ошибки нужно будет запустить цепочку компенсаторных действий в Раннере")

	input := &InputTodoData{
		Title:          "Покормить кота",
		TmpFiles:       []string{"/tmp/cat_photo.jpg", "/tmp/food_photo.png"},
		Invoker:        "Сын",
		RepeatTypeName: "ежедневно",
	}

	f := &Template{}

	// Главный процесс. Заканчиваем его ветвлением, чтобы понимать куда двигаться дальше
	f.RootBranchP("validateInput", validateInputProcessExecutor, nil).
		C("saveOrProceed", condSaveFilesOrSkipExecutor,[]string{"saveFile","saveDeferredAction"})

	f.
		IsolatedBranchP("saveFile", saveFilesPermanentlyExecutor, saveFilePermanentlyCompensator).
		P("saveDeferredAction", saveDeferredActionExecutor, saveDeferredActionCompensator).
		P("saveTodo", saveTodoExecutorWithError, saveTodoCompensator).
		P("notifyInvoker", notifyInvokerExecutor, nil)

	f.Seal()

	runner := f.NewRunner()
	_, err := runner.Run(input)

	require.Error(t, err)

	t.Log("Запускаем цепочку компенсаторных действий")
	t.Log("Поскольку ошибочное действие в сохранении тодошки, то компенсаторных действий должно быть два: удалить отложенное действие, а затем удалить вложенные файлы")
	ok, err := runner.RollbackStep()

	require.NoError(t, err)
	require.Equal(t, false, ok)

	ok, err = runner.RollbackStep()
	require.NoError(t, err)
	require.Equal(t, true, ok)

	t.Log("Автоматическое выполнение цепочки компенсаторных действий. То же, что и предыдущий пример, но одним вызовом")

	runner = f.NewRunner()

	_, err = runner.Run(input)
	require.Error(t, err)

	err = runner.Rollback()

	require.NoError(t, err)

}

// Всякие методы для теста организации кода
type InputTodoData struct {
	Title          string   // Название тодошки
	TmpFiles       []string // Названия временных файлов, предварительно загруженных на какой-то промежуточный сервер
	Invoker        string   // Имя исполнителя, которому назначена тодошка (его будем уведомлять о новой задаче)
	RepeatTypeName string   // Тип периодического напоминания исполнителя о необходимости выполнения задачи
}

func validateInputProcessExecutor(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("неверный формат входных данных. Ожидалось: *InputTodoData")
	}

	if input.Invoker == "" {
		return nil, errors.New("должен быть указан исполнитель")
	}

	return &ProcessResult{
		Value: input,
	}, nil
}

func saveFilesPermanentlyExecutor(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("внутренняя ошибка. входной параметр ДОЛЖЕН быть *InputTodoData")
	}

	filesToDelete := make([]string, len(input.TmpFiles))
	for i := range input.TmpFiles {
		fmt.Printf("Сохраняем файл %s... ВЫПОЛНЕНО\n", input.TmpFiles[i])

		filesToDelete[i] = input.TmpFiles[i]
	}

	return &ProcessResult{
		Value: input,
		CompensateParams: filesToDelete,
	},nil

}

func saveDeferredActionExecutor(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("внутренняя ошибка. входной параметр ДОЛЖЕН быть *InputTodoData")
	}

	fmt.Printf("Добавляем отложенное действие на сервер для уведомления с типом: [%s]\n", input.RepeatTypeName)

	deferredActionStorageId := int(100)

	return &ProcessResult{
		Value: input,
		CompensateParams: deferredActionStorageId,
	},nil

}

func saveTodoExecutor(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("внутренняя ошибка. входной параметр ДОЛЖЕН быть *InputTodoData")
	}

	fmt.Printf("Сохраняем тодошку [%s]\n", input.Title)

	todoStorageId := int(929292)

	return &ProcessResult{
		Value: input,
		CompensateParams: todoStorageId,
	},nil

}

func saveTodoExecutorWithError(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("внутренняя ошибка. входной параметр ДОЛЖЕН быть *InputTodoData")
	}
	fmt.Printf("Сохраняем тодошку [%s]... ПРОВАЛ!!!\n", input.Title)
	return nil, errors.New("Ошибка при сохранении тодошки")
}

func notifyInvokerExecutor(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("внутренняя ошибка. входной параметр ДОЛЖЕН быть *InputTodoData")
	}

	fmt.Printf("Уведомляем пользователя %s о новой задаче\n", input.Invoker)

	return &ProcessResult{
		Value: input,
		CompensateParams: nil,
	},nil
}

func condSaveFilesOrSkipExecutor(a any) (*ProcessResult, error) {
	input, ok := a.(*InputTodoData)
	if !ok {
		return nil, errors.New("внутренняя ошибка. входной параметр ДОЛЖЕН быть *InputTodoData")
	}

	if input.TmpFiles != nil && len(input.TmpFiles) > 0 {
		return &ProcessResult{Value: "saveFile"},nil
	}

	return &ProcessResult{Value: "saveDeferredAction"},nil

}

func saveFilePermanentlyCompensator(prms any) error {
	fmt.Printf("Удаляем файлы %a\n", prms)
	return nil
}

func saveTodoCompensator(prms any) error {
	fmt.Printf("Удаляем задачу из базы данных %a\n", prms)
	return nil
}

func saveDeferredActionCompensator(prms any) error {
	fmt.Printf("Удаляем отложенное действие-напомналку %a\n", prms)
	return nil
}
